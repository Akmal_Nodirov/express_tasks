<?php 
	session_start();
	$status = '';
	$user = '';
	if(isset($_SESSION['logged'])){
      	$status = 'logged';
      	$user = $_SESSION['username'];
    } else {
     	$status = 'guest';
    }
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name = "viewport" content ="width=device-width, initial-scale=1" >
	<title>express_task</title>
	<link rel="stylesheet" type="text/css" href="<?=$config['base']['url'];?>/web/css/bootstrap.min.css">
</head>
<body>
	<br>
	<div class="container">
		<ul class="nav nav-pills nav-justified">
			<li class='active'><a href="<?=$config['base']['url'].'?view=index'?>">Home</a></li>
			<li><a href="<?=$config['base']['url'].'?view=entering_data'?>">Entering Data</a></li>

			<?php if($status=='guest'):?>
				<li><a href="<?=$config['base']['url'].'?view=login_page'?>">Login</a></li>
			<?php endif;?>

			<?php if($status=='logged'):?>
				<li><a href="<?=$config['base']['url'].'?view=logout'?>">Logout(<?=$user?>)</a></li>
			<?php endif;?>

		</ul>

	<?php require_once($_SERVER['DOCUMENT_ROOT']."/views/pages/".$view.".php"); ?>

	</div>
	<script type="text/javascript" src="<?=$config['base']['url'];?>/web/js/jQuery.js"></script>
	<script type="text/javascript" src="<?=$config['base']['url'];?>/web/js/bootstrap.min.js"></script>

</body>
</html>