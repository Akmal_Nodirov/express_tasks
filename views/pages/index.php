<?php 

	

	$type = 'asc';
	if(isset($_GET['type'])){
		$type = $_GET['type']=='asc' ? 'desc' : 'asc';
	}

	$starting = 0;
	$page = 1;
	if(isset($_GET['page'])){
		$starting =  $_GET['page'] * 3-3;
		$page = $_GET['page'];
	}

 ?>

<br>

<h1>Task list</h1>
<table class="table table-bordered table-hover table-condensed">
	<thead>
		<tr>
			<td>User id</td>

			<td><a href="<?=$config['base']['url'].'/?view=index&sort=username&type='.$type.'&page='.$page?>">Username</td>

			<td><a href="<?=$config['base']['url'].'/?view=index&sort=email&type='.$type.'&page='.$page?>">Email</td>
			<td>Text</td>
			<td>Image</td>
			<td><a href="<?=$config['base']['url'].'/?view=index&sort=status&type='.$type.'&page='.$page?>">Status</td>
			<?php if(isset($_SESSION['logged'])):?>
				<td>Actions</td>
			<?php endif;?>
		</tr>
	</thead>
	<tbody>
		<?php if(isset($_GET['sort'])): ?>
			<?php foreach(columnSort($_GET['sort'],$_GET['type'], $starting) as $data): ?>
				<tr>
					<td><?=$data['id']?></td>
					<td><?=$data['username']?></td>
					<td><?=$data['email']?></td>
					<td><?=$data['text']?></td>
					<td><img src="<?=$data['image']?>" width="100"></td>

					<?php if($data['status'] == 0):?>
						<td>undone</td>
					<?php endif;?>

					<?php if($data['status'] == 1):?>
						<td>done</td>
					<?php endif;?>
					
					<?php if(isset($_SESSION['logged'])):?>
						<td>
							<a href="<?=$config['base']['url'].'/?view=edit_task&id='.$data['id']?>">Edit</a>
						</td>
					<?php endif;?>
				</tr>
			<?php endforeach;?>	
		<?php else: ?>	
		<?php foreach(fetchData($starting) as $data): ?>
			<tr>
				<td><?=$data['id']?></td>
				<td><?=$data['username']?></td>
				<td><?=$data['email']?></td>
				<td><?=$data['text']?></td>
				<td><img src="<?=$data['image']?>" width="100"></td>
				
				<?php if($data['status'] == 0):?>
					<td>undone</td>
				<?php endif;?>

				<?php if($data['status'] == 1):?>
					<td>done</td>
				<?php endif;?>

				<?php if(isset($_SESSION['logged'])):?>
				<td>
					<a href="<?=$config['base']['url'].'/?view=edit_task&&id='.$data['id']?>">Edit</a>
				</td>
				<?php endif;?>
			</tr>
		<?php endforeach;?>
		<?php endif; ?>	
	</tbody> 
</table>

<div style="display: flex; justify-content: center;">
<nav aria-label="Page navigation">
  <ul class="pagination">

  	<?php $active = '';?>
  	<?php $prev = $page-1; if(($page-1) <= 1){$prev = 1;}?>

    <li>
      <a href="<?=$config['base']['url'].'/?view=index&page='.$prev?>" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>

    <?php for($i = 1; $i<=pagination(); $i++):?>
    <li class="<?php if($i==$page){echo 'active';}?>"><a href="<?=$config['base']['url'].'/?view=index&page='.$i?>"><?=$i?></a></li>
	<?php endfor;?>

	<?php $next = $page+1; if(($page+1) >= $i){$next = $i-1;}?>

    <li>
      <a href="<?=$config['base']['url'].'/?view=index&page='.$next?>" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav>
</div>