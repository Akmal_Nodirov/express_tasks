<?php

	if(isset($_POST['submit_button'])){

		$username = $_POST['username'];
		$email = $_POST['email'];
		$task = $_POST['text_task'];
		$image = $_FILES['image_file'];
    $image_status = true;
    $uploadfile = '';

    if(isset($image) && empty($image['error'])){
      $uploaddir = "web/uploads/";
      $uploadfile = $uploaddir . basename($_FILES['image_file']['name']);
      $image_status = move_uploaded_file($_FILES['image_file']['tmp_name'], $uploadfile);
    }

    if($username && $email && $task){
		
    		if(inserting(table('tasks'),['username'=>$username,'email'=>$email,'text'=>$task, 'image' => $uploadfile], $image_status)){
          header("Location: ".$config['base']['url'].'?view=index');  
          exit;
    		} else {
    			$_SESSION['error'] = 'There was an error';	
    		}

    } else {
      $_SESSION['not_full'] = 'please fill all required fields';  
    }
		

	}
?>
<?php if(isset($_SESSION['save'])): ?>
	<div class="alert alert-success">
		<?php
			echo $_SESSION['save'];
			unset($_SESSION['save']);
		?>
	</div>
<?php elseif(isset($_SESSION['save'])):?>
	<div class="alert alert-danger">
		<?php 
			echo $_SESSION['error'];
			unset($_SESSION['error']);
		?>
	</div>
<?php elseif(isset($_SESSION['not_full'])):?>
  <div class="alert alert-danger">
    <?php 
      echo $_SESSION['not_full'];
      unset($_SESSION['not_full']);
    ?>
  </div>
<?php endif;?>

<h2>Creating a new task</h2>
<div class="col-md-7">
<form class="form-horizontal" action="?view=entering_data" method="POST" enctype="multipart/form-data">

  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="username" placeholder="Email" name="username">
    </div>
  </div>

  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Email</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="email" placeholder="email" name="email">
    </div>
  </div>

   <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Text task</label>
    <div class="col-sm-10">
      <textarea type="text" class="form-control" id="task" placeholder="task" rows="6" name="text_task"></textarea>
    </div>
  </div>

   <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Image</label>
    <div class="col-sm-10">
      <input type="file" class="form-control" id="task" placeholder="task" name="image_file">
    </div>
  </div>

  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default" name="submit_button">Enter</button>
    </div>
  </div>
</form>
</div>