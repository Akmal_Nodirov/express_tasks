<?php
	
	$model_id = $_GET['id'];
	$uploaded_image = false;

?>


<?php

	if(isset($_POST['update'])){

		$username = $_POST['username'];
		$email = $_POST['email'];
		$task = $_POST['text_task'];
		$image = $_FILES['image_file'];
		$status = $_POST['status'];
	    $image_status = true;
	    $uploadfile = '';

	    if(isset($image) && empty($image['error'])){

	    	if(editData($model_id)[0]->image){
	    		unlink(editData($model_id)[0]->image);
	    	}

	      	$uploaddir = "web/uploads/";
	      	$uploadfile = $uploaddir . basename($_FILES['image_file']['name']);
	      	$image_status = move_uploaded_file($_FILES['image_file']['tmp_name'], $uploadfile);

	    } else if(editData($model_id)[0]->image){
	    	$uploadfile = editData($model_id)[0]->image;
	    }

		
		if(updating(table('tasks'),['username'=>$username,'email'=>$email,'text'=>$task, 'status' => $status, 'image' => $uploadfile], $model_id)){
			$_SESSION['updated'] = 'data has been updated successfully';
			header("Location: ".$config['base']['url'].'?view=index');	
		} else {
			$_SESSION['error'] = 'There was an error';	
		}		
		

	}
?>

<h2>Editing task a new task</h2>

<div class="col-md-7">
<form class="form-horizontal" action="" method="POST" enctype="multipart/form-data">

  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="username" placeholder="Email" name="username" value="<?=editData($model_id)[0]->username?>">
    </div>
  </div>

  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Email</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="email" placeholder="email" name="email" value="<?=editData($model_id)[0]->email?>">
    </div>
  </div>

   <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Text task</label>
    <div class="col-sm-10">
      <textarea type="text" class="form-control" id="task" placeholder="task" rows="6" name="text_task"><?=editData($model_id)[0]->text?></textarea>
    </div>
  </div>

   <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Image</label>
    <div class="col-sm-10">
      <input type="file" class="form-control" id="task" placeholder="task" name="image_file">
    </div>
  </div>

  <?php $checked = ''; if(editData($model_id)[0]->status==1){ $checked='checked';}?>

    <label for="inputPassword3" class="col-sm-2 control-label">Task status</label>
    <div class="col-sm-10">
      <input type="checkbox" class="status" id="status" value=1 name="status" <?=$checked?> >
    </div>


  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default" name="update">Enter</button>
    </div>
  </div>
</form>
</div>