<?php

	/**
	 * Connecting to DB
	 */
	function connection(){
		$mysqli = new mysqli($GLOBALS['config']['db']['host'],$GLOBALS['config']['db']['username'],$GLOBALS['config']['db']['password'],$GLOBALS['config']['db']['dbname']);
		return $mysqli;
	}

	/**
	 * get the table name
	 */
	function table($table){
		global $config;
		return $config['db']['table_prefix'].$table;
	}

	/**
	 *Fetch data
	 */
	function fetchData($starting=0){
		$db = connection();

		$result = $db->query('select * from '.table('tasks'). ' limit '.$starting.', 3');
		$data  = [];
		$i = 0;


		while($row = $result->fetch_array()){
			$data[] = $row;
		}

		return $data;
	}

	/**
	 * Sorting by column names
	 */

	function columnSort($variable,$type, $starting=0){
		$db = connection();
		$result = $db->query('SELECT * FROM '.table('tasks').' ORDER BY '. $variable .' '.$type.' limit '.$starting.', 3');

		while($row = $result->fetch_array()){
			$data[] = $row;
		}

		return $data;
	}


	function inserting($table, $variables){
		$column = '';
		$value = '';
		$db = connection();

		foreach($variables as $key=>$v){
			$column .= $db->real_escape_string($key).',';
			$value .= '"'.$db->real_escape_string($v).'",';
		}

		$column = mb_substr($column,0,-1);
		$value = mb_substr($value,0,-1);

		$query = "INSERT INTO ".$table."(".$column.") VALUES(".$value.")";
		
		if($db->query($query))
			return true;

		return false;
	}

	function login($username, $password){

		$result = '';
		$db = connection();
		$query = 'select * from express_user where username= "'.$db->real_escape_string($username). '" and password='.$db->real_escape_string($password);

		if($db->query($query) && ($db->query($query))->num_rows > 0){
			$result = ['logged' => 'logged', 'username' => $username];
		} else {
			$result = ['logged' => 'fail', 'username' => $username];
		}

		return $result;

	}


	function editData($id){
		$db = connection();
		$result = $db->query('select * from '.table('tasks').' where id='.$db->real_escape_string($id));
		$data  = [];
		$i = 0;


		while($row = $result->fetch_object()){
			$data[] = $row;
		}

		return $data;
	}

	function updating($table, $variables, $id){
		$column = '';
		$update = '';
		$db = connection();

		foreach($variables as $key=>$v){

			$update .= $db->real_escape_string($key). '="'.$db->real_escape_string($v). '",'; 
		}

		$update = mb_substr($update,0,-1);
		$query = "UPDATE ".$table." SET ".$update. " where id=".$db->real_escape_string($id);
		
		if($db->query($query))
			return true;

		return false;
	}

	function pagination(){

		$db = connection();
		$query = ('select * from express_tasks');
		$pages = ($db->query($query))->num_rows;
		$result = ceil($pages/3);
		return $result;
	}



?>