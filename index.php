 
<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	include_once('config.php');
	include_once('models/db.php');
	include_once('helpers/file_upload.php');

	$view = $_GET['view'] ?? 'index';
	
	require_once($_SERVER['DOCUMENT_ROOT']."/views/layouts/main.php");

?>
